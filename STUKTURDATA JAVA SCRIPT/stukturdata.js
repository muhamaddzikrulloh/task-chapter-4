// var diskon = 500; // Global scope
// if (true) {
//   var diskon = 300; // Global scope
// }
// console.log(diskon);
// // Output: 300
// // karena var adalah global scope
// /* Sebelum ada ES6, solusinya membuat function
//     scope -> local scope */
// var diskon = 500; // Global scope
// function diskonScope() {
//   var diskon = 300; // Local scope
//   console.log(diskon); // Output: 300
// }
// diskonScope();
// console.log(diskon); // Output: 500
// redesign daan redeclear
// var name; // Declaration
// console.log(name); // Output: undefined
// name = "Bot"; // Assignment
// console.log(name); // Output: Bot
// var name = "Bot Sabrina"; // Redeclared and Reassigned
// console.log(name); // Output: Bot Sabrina

// let nama = "dz";
// let nama = "ajis";
// console.log(nama);
// output eror karena tidak bisa menggunakan variabel lagi let akan tetapi berbeda dengan var

// var planet1 = "mars";
// planet1 = "juko";
// console.log(planet1);
// let planet1 = "eko";
// console.log(planet1);
// var nama = "koki";
// console.log(`halo namaku ${nama} salken ya!`);

// ARRAY TASK PERTEMUAN 1 STUKTUR DATA JAVA SCRIPT
// let mobil = ["lambo", "avanza", "kijang"];
// mobil.unshift("sedan");
// console.log(mobil);

// const mobil = ["sedan", "lambo", "kijang"];
// mobil.forEach(function (jumlah) {
//   console.log(jumlah);
// });

// const nomor = [8, 9, 10, 11, 12];
// const nomoritem = nomor.map(function (z) {
//   return z / 10;
// });
// console.log(nomoritem);
