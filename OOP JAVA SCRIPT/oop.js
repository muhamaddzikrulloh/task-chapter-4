// function diskon(x) {
//     let musimPandemik = (x * 30)/100
//     return musimPandemik
//     }
//     let sale = diskon(20000)
//     console.log(sale)

// const volTabung = (r, t) => 3.14 * r ** 2 * t;
// console.log("Volume Tabung:", volTabung(10, 4));

// TASK 1
// DECLARATION FUNCTION

// function luasPersegi(p, l) {
//   return p * l;
// }
// console.log(`luas Persegi:`, luasPersegi(20, 21));

// function kelilingPersegi(p, l) {
//   return 2 * p + l;
// }
// console.log(`keliling persegi:`, kelilingPersegi(20, 21));

// ARROW FUNCTION

// const luasPersegi = (P, L) => P * L;
// console.log("luas persegi:", luasPersegi(2, 3));

// const kelilingPersegi = (P, L) => 2 * (P + L);
// console.log("keliling persegi:", kelilingPersegi(2, 3));

class manusia {
  static apakahKamuHidup = true;
  static apakahKamuMati = false;
  constructor(nama, alamat) {
    this.nama = nama;
    this.alamat = alamat;
  }

  ucapan() {
    console.log(`halo bro namaku ${this.nama}`);
  }
}
console.log(manusia.apakahKamuHidup);

manusia.prototype.baik = function (nama) {
  console.log(`halo, ${nama}, im ${this.nama}`);
};
manusia.destory = function (thing) {
  console.log(`manusia adlah aku ${thing}`);
};

let mj = new manusia("dzikril", "bandung");
console.log(mj);
console.log(mj instanceof manusia);
console.log(mj.ucapan());
console.log(mj.baik("jokowi"));
console.log(manusia.destory("jakrta city"));
